<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{site}', 'HomeController@dash')->name('home.show')->middleware('auth');
//Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/register', 'RegisterController@index')->name('register');
Route::get('/register/rrss', 'RegisterController@rrss')->name('rrss');
// 	Route::get('upgrade', function () {
// 		return view('pages.upgrade');
// 	})->name('upgrade');
// });

Route::group(['middleware' => 'auth'], function () {
	
	
	
	Route::resource('user', 'UserController', ['except' => ['show']]);
	
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	
	Route::get('/register/rrss', ['as'=> 'register.rrss', 'uses' =>'RegisterController@rrss']);

	Route::post('/register/save-rrss', ['as'=> 'register.save-rrss', 'uses' =>'RegisterController@save_rrss']);
});


