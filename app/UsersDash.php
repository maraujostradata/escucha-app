<?php

namespace App;

#use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UsersDash extends Pivot
{
    //
    protected $table ="users_dashes";
    protected $primaryKey = 'user_dash_id';
    public $incrementing = true;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    
}
