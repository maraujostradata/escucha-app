<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboards extends Model
{
    //
    protected $table = 'dashboards';
    protected $primaryKey = 'dashboard_id';
    public $incrementing = true;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    
    public function user()
    {
        // return $this->belongsToMany('App\"dash_id"', "users_dashes", "dash_id","dashboard_id");
        // return $this->belongsToMany('App\User');
        //return $this->belongsToMany('App\User')->using('App\UsersDash');
        
        return $this->belongsToMany('App\User', 'users_dashes','dash_id','user_id')->using('App\UsersDash');
    }

}
