<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialNetworks extends Model
{
    //
    protected $primaryKey = 'social_network_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_network', 'user_name_network','url_network', 'user_id'
    ];
    
    public function user()
    {
        return $this->hasOne('App\User');
    }

}
