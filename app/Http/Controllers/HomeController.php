<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Dashboards;
use App\UsersDash;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $item_menu = [];

        $user_id = Auth::user()->user_id;

        // $iframes = App\Dashboards::all()->user()->where('user_id', $user_id)->get();
        // $user_dash = UsersDash::where('user_id', $user_id)->get();
        //$dash = Dashboards::all();
        // $user = User::where("user_id",$user_id)->get();
        $users = User::where('user_id', $user_id)->get();
        foreach ($users as $u) {
            foreach ($u->dashboards as $dash) {

                array_push($item_menu,$dash);
            }
        }
        

        return view('dashboard',
        [
            'item_menu'=> $item_menu,
            'frame'=> $item_menu,
        ]);
    }

    public function dash($site){
        
        $item_menu = [];

        $user_id = Auth::user()->user_id;
        $dashes = Dashboards::where('slug', $site)->get();
        
        $users = User::where('user_id', $user_id)->get();

        $frame = User::find($user_id)->dashboards()->where('slug', $site)->get();

        foreach ($users as $u) {
            foreach ($u->dashboards as $dash) {

                array_push($item_menu,$dash);
            }
        }
        return view('dashboard',
        [
            'item_menu'=> $item_menu,
            'frame'=> $frame,

        ]);
    }
}
