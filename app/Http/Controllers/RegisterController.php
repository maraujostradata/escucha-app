<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\SocialNetworks;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //

    public function index(){
        return view('auth.register');
    }

    public function rrss(){
        $user= Auth::user();
        return view('auth.register-rrss', ['user' => $user]);
    }
    
    public function save_rrss(){
        $user= Auth::user();
        $url_ig = "https://www.instagram.com/";
        $url_fb = "https://www.facebook.com/";
        $arr = request()->get("rrss");
        $arr_name = request()->get("name_rss");
        $i = 1;
        // \Debugbar::info($arr);
        // \Debugbar::info($arr_name[$i]);
        foreach ($arr as $a) {
            
            $url_network = $arr_name[$i] == "instagram" ? $url_ig.$a : $url_fb.$a;
            SocialNetworks::create([
                'name_network' => $arr_name[$i],
                'user_name_network' => $a,
                'url_network' => $url_network,
                'user_id' => $user->user_id,
            ]);
            $i+=1;
        }

        return view('auth.register-rrss', [ 'user' => $user]);

        
        
    }
}
