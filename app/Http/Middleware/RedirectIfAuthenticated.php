<?php

namespace App\Http\Middleware;
use Barryvdh\Debugbar\Facade;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;


class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // \Debugbar::enable();
        
       
        if (Auth::guard($guard)->check()) {
            return redirect(RouteServiceProvider::RIGISTER_RRSS);
        }
        // \Debugbar::disable();
        
        return $next($request);
    }
}
