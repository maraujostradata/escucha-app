function blankSpace(str) {
    var noValido = /\s/;
    return noValido.test(str);
}