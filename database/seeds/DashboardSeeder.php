<?php

use Illuminate\Database\Seeder;

class DashboardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('dashboards')->insert([
            [
                'name_dash' => 'Bata',
                'slug' => 'bata-1',
                'iframe' => '<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiZTIzMTdhODItMWE4Zi00NGNiLWFiZGQtYTE3ZTI2YzQ4NDcxIiwidCI6ImNhOWFlMzkxLWIxMGUtNDI1Ny1iMjc5LWU3MzI4OTJhODZjMyIsImMiOjR9" frameborder="0" allowfullscreen></iframe>',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name_dash' => 'Bata',
                'slug' => 'bata-2',
                'iframe' => '<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiODU0YTNkMzgtNGE1Yy00YzliLWJjNmQtYjU3YmZkN2RkZjdiIiwidCI6ImNhOWFlMzkxLWIxMGUtNDI1Ny1iMjc5LWU3MzI4OTJhODZjMyIsImMiOjR9" frameborder="0" allowfullscreen></iframe>',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name_dash' => 'Bata',
                'slug' => 'bata-3',
                'iframe' => '<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiZWRiMDZmYjMtNGY4OS00MzAzLWI3MzktODNlYzAyMTU3YmRhIiwidCI6ImNhOWFlMzkxLWIxMGUtNDI1Ny1iMjc5LWU3MzI4OTJhODZjMyIsImMiOjR9" frameborder="0" allowfullscreen></iframe>',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name_dash' => 'EIA',
                'slug' => 'eia',
                'iframe' => '<iframe class="embed-responsive-item" src="https://app.powerbi.com/view?r=eyJrIjoiZWQ1ZTVlYjktZjhjNC00ZmQwLWFjNjAtZGJlYmFhNTAxMDIzIiwidCI6ImNhOWFlMzkxLWIxMGUtNDI1Ny1iMjc5LWU3MzI4OTJhODZjMyIsImMiOjR9" frameborder="0" allowfullscreen></iframe>',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
