<?php

use Illuminate\Database\Seeder;

class UserDashSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users_dashes')->insert([
        [
            'user_id' => 1,
            'dash_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'user_id' => 1,
            'dash_id' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'user_id' => 1,
            'dash_id' => 3,
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'user_id' => 1,
            'dash_id' => 4,
            'created_at' => now(),
            'updated_at' => now()
        ],
        ]);

    }
}
