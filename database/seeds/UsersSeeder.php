<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        //
        DB::table('users')->insert([
        [
            'name' => 'admin',
            'email' => 'admin@admin.co',
            'email_verified_at' => now(),
            'social_networks_active' => 1,
            'password' => Hash::make('admin'),
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'manuel',
            'email' => 'manuel.araujo@stradata.com.co',
            'email_verified_at' => now(),
            'social_networks_active' => 0,
            'password' => Hash::make('19200130'),
            'created_at' => now(),
            'updated_at' => now()
        ]]);
    }
}
