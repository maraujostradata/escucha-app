@extends('layouts.app2', ['class' => 'container-fluid', 'title' => __('Escucha')])

@section('content')


<div class="row login">
  <div class="col-md-5 text-center form">
    <img class="logo" src="{{ asset('material') }}/img/app/logo.png" alt="Escucha logo">
    <h4>Ingrese a su cuenta de Stradata</h4>
    <div id="register-href">

      <p>Necesita una cuenta?
        <a href="{{ route('register') }}" onclick="changeFormRegister()">Registrate</a>
  
      </p>
    </div>
    

    <br>
    <div id="form-change">
      

      <form class="form" method="POST" action="{{ route('login') }}" id="form-login">
        @csrf
        <div class="input-group checkbox mb-3">
          <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} input-label">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">email</i>
                </span>
              </div>
              <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}"
                value="{{ old('email', 'admin@admin.co') }}" required>
            </div>
            @if ($errors->has('email'))
            <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
              <strong>{{ $errors->first('email') }}</strong>
            </div>
            @endif
          </div>
          <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3 input-label">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">lock_outline</i>
                </span>
              </div>
              <input type="password" name="password" id="password" class="form-control"
                placeholder="{{ __('Contraseña') }}" value="{{ !$errors->has('password') ? "admin" : "" }}" required>
            </div>
            @if ($errors->has('password'))
            <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
              <strong>{{ $errors->first('password') }}</strong>
            </div>
            @endif
          </div>

          <div class="input-group checkbox mb-3 form-check mr-auto ml-3 mt-3">
          </div>
          <div class="form-check mr-auto ml-3 mt-3">
            <label class="form-check-label">
              <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
              {{ __('Remember me') }}
              <span class="form-check-sign">
                <span class="check"></span>
              </span>
            </label>
          </div>


        </div>
        <button mat-button type="submit" class="btn btn-lg btn-success btn-block">Ingresar</button>

      </form>

    </div>

  </div>

  <div class="col-md-7 bg d-none d-md-block" id="slider">
    <div class="support_btn">
      <a href="https://tawk.to/chat/5b57444ee21878736ba2432d/default" target="_blank">Contactar soporte</a>
    </div>
  </div>

</div>

@endsection