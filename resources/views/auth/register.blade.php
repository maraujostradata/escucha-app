@extends('layouts.app2', ['class' => 'off-canvas-sidebar', 'title' => __('Registro')])

@section('content')
<div class="row login">
  <div class="col-md-5 text-center form">
    <img class="logo" src="{{ asset('material') }}/img/app/logo.png" alt="Escucha logo">
    <h4>Ingrese a su cuenta de Stradata</h4>
    <div id="register-href">

      <p>Ya tienes una cuenta?
        <a href="{{ route('login') }}">Inicia sesión</a>

      </p>
    </div>


    <br>
    <div id="form-change">


      <form class="form" method="POST" action="{{ route('register') }}" id="form-register">
        @csrf
        <div class="input-group mb-3">
          <div class="bmd-form-group {{ $errors->has('name') ? ' has-danger' : '' }} input-label">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">person</i>
                </span>
              </div>
              <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre') }}" required>
            </div>
            @if ($errors->has('name'))
            <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
              <strong>{{ $errors->first('name') }}</strong>
            </div>
            @endif
          </div>
          <div class="bmd-form-group{{ $errors->has('last_name') ? ' has-danger' : '' }} input-label mt-3">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">person</i>
                </span>
              </div>
              <input type="text" name="last_name" class="form-control" placeholder="{{ __('Apellido') }}">
            </div>
            @if ($errors->has('last_name'))
            <div id="last_name-error" class="error text-danger pl-3" for="last_name" style="display: block;">
              <strong>{{ $errors->first('last_name') }}</strong>
            </div>
            @endif
          </div>
          <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} input-label mt-3">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">email</i>
                </span>
              </div>
              <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" required>
            </div>
            @if ($errors->has('email'))
            <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
              <strong>{{ $errors->first('email') }}</strong>
            </div>
            @endif
          </div>
          <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3 input-label">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="material-icons">lock_outline</i>
                </span>
              </div>
              <input type="password" name="password" id="password" class="form-control"
                placeholder="{{ __('Contraseña') }}" value="{{ !$errors->has('password') ? "" : "" }}" required>
            </div>
            @if ($errors->has('password'))
            <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
              <strong>{{ $errors->first('password') }}</strong>
            </div>
            @endif
          </div>


        </div>
        <button mat-button type="submit" class="btn btn-lg btn-success btn-block">Registrate</button>

      </form>

    </div>

  </div>

  <div class="col-md-7 bg d-none d-md-block" id="slider">
    <div class="support_btn">
      <a href="https://tawk.to/chat/5b57444ee21878736ba2432d/default" target="_blank">Contactar soporte</a>
    </div>
  </div>

</div>
@endsection