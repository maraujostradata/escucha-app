@extends('layouts.app2', ['class' => 'off-canvas-sidebar', 'title' => __('Registro redes sociales')])

@section('content')
<div class="row login">
  <div class="col-md-5 text-center form">
    <img class="logo" src="{{ asset('material') }}/img/app/logo.png" alt="Escucha logo">
    <h3 class="margin--5 gray-dark">Bienvenido {{$user->name}} {{$user->last_name}}</h3>
    <br>
    <p class="green-aml">El siguiente paso es registrar tus redes sociales</p>
    <div id="form-change">


      <form id="formSaveRRSS" class="form" method="POST" action="{{ route('register.save-rrss') }}">
        @csrf
        <div id="formulario" class="input-group mb-3">
          <div class="row width-400">
            <div class="col-md-8">
              <div class="bmd-form-group width-100">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i id="icon_1" class="material-icons">done</i>
                    </span>
                  </div>
                  <input type="text" name="rrss[1]" class="form-control" placeholder="Facebook / Instagram">
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <select class="form-control margin-top--5 width-100" name="name_rss[1]" id="name_rss">
                <option value="facebook">Facebook</option>
                <option value="instagram">Instagram</option>
              </select>
            </div>
            <div class="col-md-1 add_icon_1">
              <a onclick="addNewInput(1)">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text hover-greend  cursor-pointer">
                      <i class="material-icons">add</i>
                    </span>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>


        <button mat-button type="button" class="btn btn-lg btn-success btn-block" id="saveBtn">Guardar</button>


      </form>

    </div>

  </div>

  <div class="col-md-7 bg d-none d-md-block" id="slider">
    <div class="support_btn">
      <a href="https://tawk.to/chat/5b57444ee21878736ba2432d/default" target="_blank">Contactar soporte</a>
    </div>
  </div>

</div>
@endsection

@push('js')
<script>
  
  
  function addNewInput(i) {
    var form = $("#formulario");
    
    $('input[name^="rrss['+i+']"]').each(function() {
      if ($(this).val() != ""){
        if(blankSpace($(this).val())){
          md.showNotificationCustom('top','left','warning','warning','El nombre de la red social <br> no debe contener espacios en blanco');

        }else{
          $(".add_icon_"+i).css("display","none");
          $("#icon_"+i).html('done_all');
          i+= 1
          form.append(
            '<div class="row width-400">\
              <div class="col-md-8">\
                <div class="bmd-form-group  width-100">\
                  <div class="input-group">\
                    <div class="input-group-prepend">\
                      <span class="input-group-text">\
                        <i id="icon_'+i+'" class="material-icons">done</i>\
                      </span>\
                    </div>\
                    <input type="text" name="rrss['+i+']" class="form-control" placeholder="Facebook / Instagram">\
                  </div>\
                </div>\
              </div>\
              <div class="col-md-3">\
                <select class="form-control margin-top--5 width-100" name="name_rss['+i+']" id="name_rss">\
                  <option value="facebook">Facebook</option>\
                  <option value="instagram">Instagram</option>\
                </select>\
              </div>\
              <div class="col-md-1 add_icon_'+i+'">\
                <a href="#" onclick="addNewInput('+i+')">\
                  <div class="input-group">\
                    <div class="input-group-prepend">\
                      <span class="input-group-text hover-greend  cursor-pointer">\
                        <i class="material-icons">add</i>\
                      </span>\
                    </div>\
                  </div></a>\
              </div>\
            </div>');
          
        }
      }else{
        md.showNotificationCustom('top','left','warning','warning','Debe escribir la red social para añadir otra');
      }
    });
    
  }

$("#saveBtn").click(function(){
  var map =[];
  $('input[name^="rrss"]').each(function() {
    if ($(this).val() != ""){

      map.push($(this).val())
    }
  });
  console.log(map)
  if (map.length == 0 ){
    // Notificacion custom de material
    md.showNotificationCustom('top','left','warning','warning','Debe ingresar <br> al menos una red social')

  }else{
    $("#formSaveRRSS").submit()
    //alert("envie")
    
  }

})
</script>

@endpush