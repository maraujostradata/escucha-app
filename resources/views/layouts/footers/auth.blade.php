<footer class="footer">
  <div class="container">
      <nav class="float-left">
      <ul>
          <li>
          <a href="https://aml.stradata.co/">
              {{ __('Stradata AML') }}
          </a>
          
      </ul>
      </nav>
      <div class="copyright float-right">
      &copy;
      <script>
          document.write(new Date().getFullYear())
      </script>, made by
      <a href="https://stradata.co/" target="_blank">Stradata</a>
      </div>
  </div>
</footer>