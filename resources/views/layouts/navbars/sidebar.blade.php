<div class="sidebar" data-color="danger" data-background-color="white">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo ">
    <a href="#" class="simple-text logo-normal">
      {{ env('APP_NAME') }}
    </a>
  </div>

  <div class="sidebar-wrapper">
    <ul class="nav">
      {{-- <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
      <a class="nav-link" href="{{ route('home') }}">
        <i class="material-icons">dashboard</i>
        <p>{{ __('Dashboard') }}</p>
      </a>
      </li> --}}


      <li class="nav-item{{ $activeMenu == 'dashboard'  ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#dashboard-profile" aria-expanded="false">
          <i class="material-icons">dashboards</i>
          <p>{{ __('Dashboard') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse{{ $activeMenu == 'dashboard'  ? ' show' : '' }}" id="dashboard-profile">
          <ul class="nav">
            @foreach ($menu as $f)
            
            <li class="nav-item{{ $site->slug ==  $f->slug ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('home.show',["site"=>$f->slug]) }}">
                {{-- <span class="sidebar-mini"> UP </span> --}}
                <span class="sidebar-normal">{{ $f->name_dash }} </span>
              </a>
            </li>
            @endforeach
          </ul>
        </div>
      </li>
      {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
      <a class="nav-link" href="{{ route('table') }}">
        <i class="material-icons">content_paste</i>
        <p>{{ __('Table List') }}</p>
      </a>
      </li>
      <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
          <p>{{ __('Typography') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('map') }}">
          <i class="material-icons">location_ons</i>
          <p>{{ __('Maps') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li> --}}
      {{-- <li class="nav-item{{ $activePage == 'language' ? ' active' : '' }}">
      <a class="nav-link" href="{{ route('language') }}">
        <i class="material-icons">language</i>
        <p>{{ __('RTL Support') }}</p>
      </a>
      </li>
      <li class="nav-item active-pro{{ $activePage == 'upgrade' ? ' active' : '' }} bg-danger">
        <a class="nav-link text-white" href="{{ route('upgrade') }}">
          <i class="material-icons">unarchive</i>
          <p>{{ __('Upgrade to PRO') }}</p>
        </a>
      </li> --}}
    </ul>
  </div>
</div>