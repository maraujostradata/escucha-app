@extends('layouts.app', ['site' => $frame[0], 'activeMenu' => 'dashboard', 'titlePage' => __('Dashboard'),
'menu'=>$item_menu])

@section('content')




<div class="content">
  {{-- <div class="container-fluid"> --}}
  <div class="row">
    {{-- <div class="col-md-12">
      <button onclick="reloadreport()">Click me to reload</button>
      
    </div> --}}
    <div class="col-md-12">
      <div class="embed-responsive embed-responsive-16by9">

        {{-- @foreach ($frames as $f)
        {!! $f->iframe !!}
        @endforeach --}}

        {!! $frame[0]->iframe !!}
        {{-- <div id="reportContainer" class="embed-responsive-item"></div> --}}
      </div>

    </div>

  </div>
  {{-- <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">content_copy</i>
            </div>
            <p class="card-category">Used Space</p>
            <h3 class="card-title">49/50
              <small>GB</small>
            </h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons text-danger">warning</i>
              <a href="#pablo">Get More Space...</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">store</i>
            </div>
            <p class="card-category">Revenue</p>
            <h3 class="card-title">$34,245</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">date_range</i> Last 24 Hours
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">info_outline</i>
            </div>
            <p class="card-category">Fixed Issues</p>
            <h3 class="card-title">75</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">local_offer</i> Tracked from Github
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
              <i class="fa fa-twitter"></i>
            </div>
            <p class="card-category">Followers</p>
            <h3 class="card-title">+245</h3>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">update</i> Just Updated
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card card-chart">
          <div class="card-header card-header-success">
            <div class="ct-chart" id="dailySalesChart"></div>
          </div>
          <div class="card-body">
            <h4 class="card-title">Daily Sales</h4>
            <p class="card-category">
              <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> updated 4 minutes ago
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-chart">
          <div class="card-header card-header-warning">
            <div class="ct-chart" id="websiteViewsChart"></div>
          </div>
          <div class="card-body">
            <h4 class="card-title">Email Subscriptions</h4>
            <p class="card-category">Last Campaign Performance</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> campaign sent 2 days ago
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-chart">
          <div class="card-header card-header-danger">
            <div class="ct-chart" id="completedTasksChart"></div>
          </div>
          <div class="card-body">
            <h4 class="card-title">Completed Tasks</h4>
            <p class="card-category">Last Campaign Performance</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">access_time</i> campaign sent 2 days ago
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-tabs card-header-primary">
            <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                <span class="nav-tabs-title">Tasks:</span>
                <ul class="nav nav-tabs" data-tabs="tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#profile" data-toggle="tab">
                      <i class="material-icons">bug_report</i> Bugs
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#messages" data-toggle="tab">
                      <i class="material-icons">code</i> Website
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#settings" data-toggle="tab">
                      <i class="material-icons">cloud</i> Server
                      <div class="ripple-container"></div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="tab-content">
              <div class="tab-pane active" id="profile">
                <table class="table">
                  <tbody>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="" checked>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Sign contract for "What are conference organizers afraid of?"</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept
                        through metro Detroit
                      </td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="" checked>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Create 4 Invisible User Experiences you Never Knew About</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="messages">
                <table class="table">
                  <tbody>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="" checked>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept
                        through metro Detroit
                      </td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Sign contract for "What are conference organizers afraid of?"</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="tab-pane" id="settings">
                <table class="table">
                  <tbody>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="">
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="" checked>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept
                        through metro Detroit
                      </td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" value="" checked>
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </td>
                      <td>Sign contract for "What are conference organizers afraid of?"</td>
                      <td class="td-actions text-right">
                        <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                          <i class="material-icons">edit</i>
                        </button>
                        <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                          <i class="material-icons">close</i>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="card">
          <div class="card-header card-header-warning">
            <h4 class="card-title">Employees Stats</h4>
            <p class="card-category">New employees on 15th September, 2016</p>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <th>ID</th>
                <th>Name</th>
                <th>Salary</th>
                <th>Country</th>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Dakota Rice</td>
                  <td>$36,738</td>
                  <td>Niger</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Minerva Hooper</td>
                  <td>$23,789</td>
                  <td>Curaçao</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Sage Rodriguez</td>
                  <td>$56,142</td>
                  <td>Netherlands</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Philip Chaney</td>
                  <td>$38,735</td>
                  <td>Korea, South</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div> --}}
  {{-- </div> --}}
</div>
@endsection

@push('js')
<script src="https://microsoft.github.io//PowerBI-JavaScript/demo/node_modules/jquery/dist/jquery.js"> </script>
<script src="https://microsoft.github.io//PowerBI-JavaScript/demo/node_modules/powerbi-client/dist/powerbi.js">
</script>
<script>
  $(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    md.initDashboardPageCharts();
  });
  
  var access_token = "eyJ0eXAiOiJKV1QiLCJub25jZSI6ImlOdEJuSDE0eFZUYzZJOUplS0ljOXk1LS1EREs4RjRMa3kyS3lTcjlIbVEiLCJhbGciOiJSUzI1NiIsIng1dCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSIsImtpZCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9jYTlhZTM5MS1iMTBlLTQyNTctYjI3OS1lNzMyODkyYTg2YzMvIiwiaWF0IjoxNTg4NzkyNTQ3LCJuYmYiOjE1ODg3OTI1NDcsImV4cCI6MTU4ODc5NjQ0NywiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IjQyZGdZUERZbkpKeGM3cVZhMTZPMEtIdGQwTzB6TjAyQ0wvNTRsTC9mRmFoeDB1ZUxGWUEiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IlB3ckJJIiwiYXBwaWQiOiJkYzE5ZmU3Ny1iOGQ5LTQzNTgtODU1MS0wMDQxOThhOWVhZTIiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6IlJlc3RyZXBvIFDDqXJleiIsImdpdmVuX25hbWUiOiJFbGlhbmEgTWFyw61hIiwiaXBhZGRyIjoiMTgxLjU4LjM4LjQzIiwibmFtZSI6IkVsaWFuYSBNYXLDrWEgUmVzdHJlcG8gUMOpcmV6Iiwib2lkIjoiYTVmYmM0MmUtMWY0Mi00NmVlLTlhYzEtMDRjMWY3NzNjODFmIiwicGxhdGYiOiIzIiwicHVpZCI6IjEwMDMyMDAwOTU4OUZFRDMiLCJzY3AiOiJvcGVuaWQgcHJvZmlsZSBVc2VyLlJlYWQgZW1haWwiLCJzdWIiOiJySEFOWk8ydE9xTXlQdjMxRklZcWw0aENGdk5tamFOYUphaHViUHZBN1prIiwidGlkIjoiY2E5YWUzOTEtYjEwZS00MjU3LWIyNzktZTczMjg5MmE4NmMzIiwidW5pcXVlX25hbWUiOiJlbGlhbmEucmVzdHJlcG9Ac3RyYWRhdGEuY29tLmNvIiwidXBuIjoiZWxpYW5hLnJlc3RyZXBvQHN0cmFkYXRhLmNvbS5jbyIsInV0aSI6IlFTLVhaN19yQkV1aDF1OGxzNXNuQUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbIjYyZTkwMzk0LTY5ZjUtNDIzNy05MTkwLTAxMjE3NzE0NWUxMCJdLCJ4bXNfc3QiOnsic3ViIjoiRW9hVkxiSVJmS3ZCbnRIOFptZTJFeDk2M1RNOWU3Y0lyQ2REV3pvRFhiWSJ9LCJ4bXNfdGNkdCI6MTUyNzA5OTE2OX0.W-jLhVCxGmxIMwsEfXMhb51asiyLT4dIEFbdifED-7LcfCynRX_fZIW0U9LPnKQ2NY-DXoJfP77AVgXJIcaao3qN04lA2xyfA6uUhZw4HjQnVqXdM1KArtBw6LUhOVjSewo4IxVHvoZPxV4nAFOaTn83dqDHBgjmXUA83Jg3gqz87whr-nxacrEWfbvSWrI9lJH0IZhdD4UW4yV8GTyDRrXesyiRtpYJZqUNCYcJrLE6B02-BnN0I_3B2L4T5yyxyHSOBNEEZae9wu4dKeZfCExMsCcttoK0KuIA809pXXxyLD68C_m4RMPkNmLf04zB526vNobYzrsS1vnP-rDTDg";

  var access_token_2 = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSIsImtpZCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvY2E5YWUzOTEtYjEwZS00MjU3LWIyNzktZTczMjg5MmE4NmMzLyIsImlhdCI6MTU4ODYwNTI4MiwibmJmIjoxNTg4NjA1MjgyLCJleHAiOjE1ODg2MDkxODIsImFjY3QiOjAsImFjciI6IjEiLCJhaW8iOiI0MmRnWURDZTVaN0ozTncwL2Z6Y2t4NDdtN3VUSDNMdlgyWVI4TktDM2ZXNmNadnJzeklBIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6IjdmNTlhNzczLTJlYWYtNDI5Yy1hMDU5LTUwZmM1YmIyOGI0NCIsImFwcGlkYWNyIjoiMiIsImZhbWlseV9uYW1lIjoiUmVzdHJlcG8gUMOpcmV6IiwiZ2l2ZW5fbmFtZSI6IkVsaWFuYSBNYXLDrWEiLCJpcGFkZHIiOiIxODEuNTguMzguNDMiLCJuYW1lIjoiRWxpYW5hIE1hcsOtYSBSZXN0cmVwbyBQw6lyZXoiLCJvaWQiOiJhNWZiYzQyZS0xZjQyLTQ2ZWUtOWFjMS0wNGMxZjc3M2M4MWYiLCJwdWlkIjoiMTAwMzIwMDA5NTg5RkVEMyIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6IjZJeDFmM2NxZkEyOXNsSXZ5dTJUWkZEZVQteWxNY1BScHpHU2R2eVdrWm8iLCJ0aWQiOiJjYTlhZTM5MS1iMTBlLTQyNTctYjI3OS1lNzMyODkyYTg2YzMiLCJ1bmlxdWVfbmFtZSI6ImVsaWFuYS5yZXN0cmVwb0BzdHJhZGF0YS5jb20uY28iLCJ1cG4iOiJlbGlhbmEucmVzdHJlcG9Ac3RyYWRhdGEuY29tLmNvIiwidXRpIjoiNjlhbUlsQnZDMEtXek5fZmtZU3pBQSIsInZlciI6IjEuMCIsIndpZHMiOlsiNjJlOTAzOTQtNjlmNS00MjM3LTkxOTAtMDEyMTc3MTQ1ZTEwIl19.pODb-G4jhZAH8U8a8lphLh2KLL12KlnlJ-JGVY2QIYlf3eylBCpeBhwFN1tLHMcgdhq7vG3hclWyo6m2vYvYaB1H-Lx_jU69611JVM56Y_gXb8Q0xgi0wKIAmXINZowYBL4OE3DeNsmU_v85nF01jNSrI9KECYBdH6pWKvjPHglJZeIhcdM6NY2tM6c6UyNYaaIrXWyeK1IN4HhIFEmY3DQrcxLd6NNIq4czFOKNjHnP1U_h2p5PXrUMcEz-ATmupo_23ld2hqP4DyfhNkL5BwJi9pD2Jo_Nx30tS01kBxdDr-6B_pM0IEAa2HExPCCEqLhxjicmtkctq7mFACN0kA"
  var reportID = "215e0e2a-2bc7-4165-bd31-08e9ef86986c";
  // pwr-bi-workspace - Informe ejemplo
  var reportID2 = "efd4e011-9207-42db-9e2c-629a38c80edc";
  // DEMO 1
  var reportID3 = "0fe25766-3b18-44aa-b8b1-b112c2f195cc";

  var models = window['powerbi-client'].models;
  console.log("models-->: "+models.TokenType.Embed)
  // escucha-pwrbi-ap
  var application_id = "7297646e-bcce-4f38-af71-78379fa687c9"
  var application_secret = "knQME2EB84dqaHYpU9YmjbPrAhu42vUzg0KoXXjcIyU="

  // var embedConfiguration = {
  //   type: 'report',  
  //   accessToken: access_token ,
  //   id: reportID,
  //   embedUrl: 'https://app.powerbi.com/reportEmbed'
  // }; 

  var embedConfiguration = {
    type: 'report',
    id: reportID2,
    embedUrl: 'https://app.powerbi.com/reportEmbed',
    tokenType: models.TokenType.Aad,
    accessToken: access_token
  };
  // Embed.prototype.getAccessToken = function (globalAccessToken) {
  //       var accessToken = this.config.accessToken || this.element.getAttribute(Embed.accessTokenAttribute) || globalAccessToken;
  //       if (!accessToken) {
  //           throw new Error("No access token was found for element. You must specify an access token directly on the element using attribute '" + Embed.accessTokenAttribute + "' or specify a global token at: powerbi.accessToken.");
  //       }
  //       return accessToken;
  //   };

  // Embed.prototype.setAccessToken = function (access_token) {
  //       var embedType = this.config.type;
  //       return this.service.hpm.post('/' + embedType + '/token', accessToken, { uid: this.config.uniqueId }, this.iframe.contentWindow)
  //           .then(function (response) {
  //           return response.body;
  //       })
  //           .catch(function (response) {
  //           throw response.body;
  //       });
  //   };
  
  window.onload = function () {

    var $reportContainer = $('#reportContainer');
    

    // var report = powerbi.init(); 
    var report = powerbi.embed($reportContainer.get(0), embedConfiguration); 
    // report.setAccessToken(access_token).catch(error =>{ console.log("error -> "+ error) });
    // var token = report.getAccessToken();
    // console.log("token -> "+ token)

  }
  function reloadreport(){ 

    var $reportContainer = $('#reportContainer');
    powerbi.embedNew($reportContainer.get(0), embedConfiguration);

  };
  app_id = "f1f01b3f-d2c0-449e-bd54-974491ab740b";
  work_space_id = "e8a38a94-b449-400c-9b8c-b54b2391882a";
  repot_id = "efd4e011-9207-42db-9e2c-629a38c80edc"
</script>

@endpush

{{-- GET ->
https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=dc19fe77-b8d9-4358-8551-004198a9eae2&response_type=code&redirect_uri=http://localhost:8000&response_mode=query&scope=https://graph.microsoft.com/.default

response -> 
http://localhost:8000/?code=OAQABAAIAAAAm-06blBE1TpVMil8KPQ41JVvoh_3ryF1I4n2Yatn542JPHFgxOdsVf-dusgqv5dLBGna6odZO5RRcwLqczQnotk90J9T4RR2yQeDPzNzR0dPiI-oOTZKn92ByM3r1Wc-UUTrAnXe9zDtKpLYT2Rq9UZYcvWE5OsbMkGBQEzdVpJk3UAneIi0q6-k202nBmBP66IVssTlu678nxZIM94Kxtd7U7-u1oQPXxXgSrC3ZmcqOCxGpslbLScH_z_qu-0CUbkDBL9P6Hs0sIZyzKo2CrP-l0VlQI7_ae28wYNCAOdpHEXnSi4vhtupq2Cwj5Kn9Eu09I1w9g0ctVUSFymCffHF2BbNuG5JdWOEeubBGLyKHR80s55XtH4GBZwE8bwLWdT4lWelHfvxlaCKJSNAxa4SyAsmjZfZa9LnbbrlhTCOgdsMQNnEzceU6yt6R6Fu4Sv3-HinbRuyJ1EzOEiXklJ71UwC73zpvLSEe8fFkKXinXKgiM2Ke-uAKSjoJxojBVJoxTOVPFbkmWSB5LkousZazA6WhCzLLMAirMTel-AVWR-1oCZnYCBIgYgsVuIzqYUAnulYEPka1phsam5tf93eQRqc_RTEO4g9CgwvogiAA&session_state=365fea2f-82f7-47ab-a26b-f789522a8e7c

var client_secret ="Y0.8iXNa]hA@lg@cfa6fyWwv7dDflp/G"
var code = "OAQABAAIAAAAm-06blBE1TpVMil8KPQ41JVvoh_3ryF1I4n2Yatn542JPHFgxOdsVf-dusgqv5dLBGna6odZO5RRcwLqczQnotk90J9T4RR2yQeDPzNzR0dPiI-oOTZKn92ByM3r1Wc-UUTrAnXe9zDtKpLYT2Rq9UZYcvWE5OsbMkGBQEzdVpJk3UAneIi0q6-k202nBmBP66IVssTlu678nxZIM94Kxtd7U7-u1oQPXxXgSrC3ZmcqOCxGpslbLScH_z_qu-0CUbkDBL9P6Hs0sIZyzKo2CrP-l0VlQI7_ae28wYNCAOdpHEXnSi4vhtupq2Cwj5Kn9Eu09I1w9g0ctVUSFymCffHF2BbNuG5JdWOEeubBGLyKHR80s55XtH4GBZwE8bwLWdT4lWelHfvxlaCKJSNAxa4SyAsmjZfZa9LnbbrlhTCOgdsMQNnEzceU6yt6R6Fu4Sv3-HinbRuyJ1EzOEiXklJ71UwC73zpvLSEe8fFkKXinXKgiM2Ke-uAKSjoJxojBVJoxTOVPFbkmWSB5LkousZazA6WhCzLLMAirMTel-AVWR-1oCZnYCBIgYgsVuIzqYUAnulYEPka1phsam5tf93eQRqc_RTEO4g9CgwvogiAA"
POST -> 
https://login.microsoftonline.com/common/oauth2/v2.0/token

grant_type=authorization_code&client_id=dc19fe77-b8d9-4358-8551-004198a9eae2&code=OAQABAAIAAAAm-06blBE1TpVMil8KPQ41e1nrNV7jpQdURh0_eM2Tsy4VbmYNT-11XqPj405zo2XMYIjwtSWNNohQ9gjE1fufGG_79MeT-Ceuoq7vSDwQD60ZxBGUOR_Wz6h6Qfu6t_8e4uAV5T_hwjTiVnn1MYL6xtk5LMQczsxYzKZTmOJaL_VdZ8XxNVvvdOmLyiEGkrrj7VySWWf1eH-rG35x9hJjAvH9rY1xkRuz8fktf1W7h_9-VFlYOZxrUbR5zPuRY8EC3jz6J0sDVevNivsGNNaaTRMXvOV_oim-DZfsnY7q2HjyE8MHWOr1kwL6OA4SE4mHERpQJ2FRMmu2vmRFU1yajxxdf2iOWqKh4vxvRH08ukPgdoq-TsCJyX6fHqH0AripEmemfHDXaHVTPVV9-4NFJUGiCMuDGX1lDYiqgIb40Pd4tWiB32uacaPTR3xeUBl_enJhDHUS4KFWjjKrIt0L58ZLiwaYgLsRu2lZ6AkcQybrdnI4IAWo2Xkf8ubcqWagHPZjU1BWpf1zheX7JKwcwtuWysfU2uFGcqe7gvQNLLwQkWQv8Y5XwOMbnZlm1DZCCO6w1lTUF4KNhw1C-FbMIAA&redirect_uri=http://localhost:8000&client_secret=Y0.8iXNa]hA@lg@cfa6fyWwv7dDflp/G

response -> 
{
  "token_type": "Bearer",
  "scope": "openid profile email https://graph.microsoft.com/User.Read https://graph.microsoft.com/.default",
  "expires_in": 3599,
  "ext_expires_in": 3599,
  "access_token": "eyJ0eXAiOiJKV1QiLCJub25jZSI6IlNKN0lKc3dZWUpiMXZfVXRZb0U1c0lMb1VYd21VdEVtcTMxdEZYU0FkZ1EiLCJhbGciOiJSUzI1NiIsIng1dCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSIsImtpZCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSJ9.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9jYTlhZTM5MS1iMTBlLTQyNTctYjI3OS1lNzMyODkyYTg2YzMvIiwiaWF0IjoxNTg4NjI3MjA5LCJuYmYiOjE1ODg2MjcyMDksImV4cCI6MTU4ODYzMTEwOSwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IjQyZGdZSEN6ZUZQandESDUzVWJPWEE2K1BYc3MweHZPYUhqKzdtbXpxcEg0T0gvK1VqMEEiLCJhbXIiOlsicHdkIl0sImFwcF9kaXNwbGF5bmFtZSI6IlB3ckJJIiwiYXBwaWQiOiJkYzE5ZmU3Ny1iOGQ5LTQzNTgtODU1MS0wMDQxOThhOWVhZTIiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6IlJlc3RyZXBvIFDDqXJleiIsImdpdmVuX25hbWUiOiJFbGlhbmEgTWFyw61hIiwiaXBhZGRyIjoiMTgxLjU4LjM4LjQzIiwibmFtZSI6IkVsaWFuYSBNYXLDrWEgUmVzdHJlcG8gUMOpcmV6Iiwib2lkIjoiYTVmYmM0MmUtMWY0Mi00NmVlLTlhYzEtMDRjMWY3NzNjODFmIiwicGxhdGYiOiIzIiwicHVpZCI6IjEwMDMyMDAwOTU4OUZFRDMiLCJzY3AiOiJvcGVuaWQgcHJvZmlsZSBVc2VyLlJlYWQgZW1haWwiLCJzdWIiOiJySEFOWk8ydE9xTXlQdjMxRklZcWw0aENGdk5tamFOYUphaHViUHZBN1prIiwidGlkIjoiY2E5YWUzOTEtYjEwZS00MjU3LWIyNzktZTczMjg5MmE4NmMzIiwidW5pcXVlX25hbWUiOiJlbGlhbmEucmVzdHJlcG9Ac3RyYWRhdGEuY29tLmNvIiwidXBuIjoiZWxpYW5hLnJlc3RyZXBvQHN0cmFkYXRhLmNvbS5jbyIsInV0aSI6InBmY1N5MGNqcjBHbXg2amRqWmxuQUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbIjYyZTkwMzk0LTY5ZjUtNDIzNy05MTkwLTAxMjE3NzE0NWUxMCJdLCJ4bXNfc3QiOnsic3ViIjoiRW9hVkxiSVJmS3ZCbnRIOFptZTJFeDk2M1RNOWU3Y0lyQ2REV3pvRFhiWSJ9LCJ4bXNfdGNkdCI6MTUyNzA5OTE2OX0.0na6JgJn046mrrQtE4Xv_snD1RCDxzwRfZ3iU9ZOyiTbD_9RFZ2BcHHw6BN0UxzX9Ias5x_q9-ERnRzOWFMw7AgckllauiBit_gPejStydNGcnvAV2oQy-NMdIiLLbghNHobe_5-i-QcICt0qYpqeZuFQmubBFGmLCYKcjMnG_2hEaVp5DgLh4miQIOYRJL-56x1a3hR07g1bCBHvyhSIDhvV0uiuaZU1FoWN2m3-HsYqPGmr16QPVfmdQyse-679QDqveiRJnUngXBNxXWM8kkECJImA-zCKPKq2ZGBiPrjjymLrzk1113FcsyhepznRyQhJLFT8fJzDBUdq15QFA"
}


publish app:

https://app.powerbi.com/Redirect?action=OpenApp&appId=7687060f-6075-4765-b6be-89df242b42fb&ctid=ca9ae391-b10e-4257-b279-e732892a86c3


<iframe class="embed-responsive-item" src="https://app.powerbi.com/reportEmbed?reportId=215e0e2a-2bc7-4165-bd31-08e9ef86986c&autoAuth=true&ctid=ca9ae391-b10e-4257-b279-e732892a86c3&pageName=ReportSectionaae3116298f1e3d24037&filter=icono%2Fname%20eq%20%27BATA%27&navContentPaneEnabled=false" frameborder="0" allowfullscreen></iframe>
--}}